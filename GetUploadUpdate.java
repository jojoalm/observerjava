
import Controle.ByteUpload.ByteLong;
import java.util.List;

/**
 *
 * @author Josiane Almeida - josiane_almeida@hotmail.com
 */
public interface GetUploadUpdate {
    public List<ByteLong> addByteUpload(ByteLong update);
    public ByteLong getByteUpload(ByteLong Long); 
}
