
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Josiane Almeida - josiane_almeida@hotmail.com
 */
public class ByteUpload {
private List<GetUploadUpdate> listeneres = new ArrayList<>();
private List<ByteLong> listLong = new ArrayList<>();
private List<ByteLong> list = new ArrayList<>();
ImplementUpload listener = new ImplementUpload();
private List<ByteLong> b = new ArrayList<>();

    public ByteUpload(GetUploadUpdate listener) {
        registerByteAddedListener(listener);
    }
 


  public List<ByteLong> AddLongByte(ByteLong bite){
   listLong = this.notifyByteddedListeners(bite);      
  
   return listLong;   
    }

    public List<ByteLong> GetLongByte(List<ByteLong> lista){ 
      
        lista.stream().map((u) -> {
            this.notifyByteStringListener(u);
        return u;
    }).forEachOrdered((u) -> {
        System.out.println("out-"+u);       
    });
    return lista;
      
    }
    
   public void registerByteAddedListener (GetUploadUpdate listener) {
        // Add the listener to the list of registered listeners
        this.listeneres.add(listener);
    }
    public void unregisterByteAddedListener (GetUploadUpdate listener) {
        // Remove the listener from the list of the registered listeners
        this.listeneres.remove(listener);
    }
    protected List<ByteLong> notifyByteddedListeners (ByteLong b) {
        // Notify listener add byte to upload
          
        this.listeneres.forEach(listener -> listener.addByteUpload(b));
        this.listeneres.stream().map((u) -> {
             list.add(b);
        return u;
    }).forEachOrdered((u) -> {       
    });
        return list;
    }
       
    protected ByteLong notifyByteStringListener(ByteLong b){
//    Notify listener to get Byte add
     this.listeneres.forEach(listener -> listener.getByteUpload(b));
      return b;
        
    }
}