
/**
 *
 * @author Josiane Almeida - josiane_almeida@hotmail.com
 */
public class ByteLong {
private Long bite;

    public ByteLong() {        
    }

    public Long getBite() {
        return bite;
    }

    public void setBite(Long bite) {
        this.bite = bite;
    }

    @Override
    public String toString() {
        return "ByteLong{" + "bite=" + bite + '}';
    }

    
}
